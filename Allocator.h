// ----------------------------------
// projects/c++/allocator/Allocator.h
// Copyright (C) 2018
// Glenn P. Downing
// ----------------------------------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;}                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);}

    public:
        // --------
        // typedefs
        // --------

        using      value_type = T;

        using       size_type = std::size_t;
        using difference_type = std::ptrdiff_t;

        using       pointer   =       value_type*;
        using const_pointer   = const value_type*;

        using       reference =       value_type&;
        using const_reference = const value_type&;

    private:
        // ----
        // data
        // ----

        char a[N];

        // -----
        // valid
        // -----

        /**
         * O(1) in space
         * O(n) in time
         * <your documentation>
         */
        bool valid () const {
            // <your code>
            return true;}

    public:
        // --------
        // iterator
        // --------

        class iterator {
            // -----------
            // operator ==
            // -----------

            friend bool operator == (const iterator&, const iterator&) {
                // <your code>
                return false;}                                           // replace!

            // -----------
            // operator !=
            // -----------

            friend bool operator != (const iterator& lhs, const iterator& rhs) {
                return !(lhs == rhs);}

            private:
                // ----
                // data
                // ----

                char* _p;

            public:
                // -----------
                // constructor
                // -----------

                iterator (char* p) {
                    _p = p;}

                // ----------
                // operator *
                // ----------

                char operator * () const {
                    // <your code>
                    return 0;}             // replace!

                // -----------
                // operator ++
                // -----------

                iterator& operator ++ () {
                    // <your code>
                    return *this;}

                // -----------
                // operator ++
                // -----------

                iterator operator ++ (int) {
                    iterator x = *this;
                    ++*this;
                    return x;}};

        // -----------
        // constructor
        // -----------

        /**
         * O(1) in space
         * O(1) in time
         * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
         */
        my_allocator () {
            if (N<(sizeof(T) + (2 * sizeof(int)))){
                std::bad_alloc exception;
                throw exception;
            }
            this->operator[](0) = N - sizeof(int)*2;
            this->operator[](N - sizeof(int)) = N - sizeof(int)*2;
            assert(valid());
        }

        my_allocator             (const my_allocator&) = default;
        ~my_allocator            ()                    = default;
        my_allocator& operator = (const my_allocator&) = default;

        // --------
        // allocate
        // --------

        /**
         * O(1) in space
         * O(n) in time
         * after allocation there must be enough space left for a valid block
         * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
         * choose the first block that fits
         * throw a bad_alloc exception, if n is invalid
         */
        pointer allocate (size_type num) {
             if (num<=0) {
                std::bad_alloc exception; 
                throw exception;
            }
            int amount = num * sizeof(T);
            int index = 0;
            int val = 0;
            while (index<N) {
                val = this->operator[](index);
                if (val >= amount) {
                    break;
                } 
                index += (abs(val) + sizeof(int)*2);
            }
            if(index>=N) {
                std::bad_alloc exception; 
                throw exception;
            }
            if(val<(amount+sizeof(int)*2 + sizeof(T)))
                amount = val;
            //index is where we want our new sentinel to be
            int sentinal2Index = index + amount + sizeof(int);
            int newSentinalIndex = sentinal2Index + sizeof(int);
            int remainingFreeSpace = val - amount - 2*sizeof(int);
            if (val-amount == 0)
                remainingFreeSpace = 0;
            int newSentinal2Index = newSentinalIndex + remainingFreeSpace + sizeof(int);
            
            this->operator[](index) = -1 * amount;
            this->operator[](sentinal2Index) = -1 * amount;
            if (remainingFreeSpace != 0) {
                this->operator[](newSentinalIndex) = remainingFreeSpace;
                this->operator[](newSentinal2Index) = remainingFreeSpace;
            }
            
            assert(valid());
            return reinterpret_cast<pointer>(a+index);
        }

        // ---------
        // construct
        // ---------

        /**
         * O(1) in space
         * O(1) in time
         */
        void construct (pointer p, const_reference v) {
            new (p) T(v);                               // this is correct and exempt
            assert(valid());}                           // from the prohibition of new

                /** 
        * check if the newly freed block with sentinal starting at index needs to be 
        * coalesced with adjacent blocks.
        * @return value
        * 0 = no coalescing required
        * 1 = left block needs to coalesced
        * 2 = right block needs to be coalesced
        * 3 = both left and right block needs to be coalesced
        */
        int checkCoalesce(char* index) {
            int result = 0;
            int valLeft = 0;
            if((index - sizeof(int)) >= a) 
                valLeft =get(*(index - sizeof(int)));
            if(valLeft > 0) 
                result += 1;
            int currVal = get(*index);
            int valRight = 0;
            if(index + currVal + sizeof(int)*2 < a+N)
                valRight = get(*(index + currVal + sizeof(int)*2));
            if(valRight > 0) 
                result += 2;
            return result;
        }
        
        //coalesce the two blocks that have sentinels starting at the below indexes
        void coalesce(char* leftIndex, char* rightIndex) {
            int leftVal = get(*leftIndex);
            int rightVal = get(*rightIndex);
            int newVal = leftVal + rightVal + sizeof(int)*2;
            set (*leftIndex, newVal);
            set (*(rightIndex + rightVal + sizeof(int)), newVal);
            set (*rightIndex, 0);
            set (*(rightIndex-sizeof(int)), 0);
        }
    
        //returns the integer value of the sentinel at location c in the char array
        int& get (char& c) const 
        {
            return *reinterpret_cast<int*>(&c);        
        }
    
        //sets the integer value of the sentinel at location c in the char array to val
        int&  set (char& c, int val) {
            return *(reinterpret_cast<int*>(&c)) = val;        
        }

        //returns a pointer to the pth busy block     
        pointer getNthBusyBlock(size_t p) {
            // iterate through char array, 
            // have an index,
            char *p1 = a;
            //std::cout << *reinterpret_cast<int*>(p1) << std::endl;
            int val = 0;
            int busyBlockIndex = 0;
            p = p * (-1);
            while(p1 < (a+N)) {
                val = get(*p1);
                if(val < 0) {
                    ++busyBlockIndex;
                    if(busyBlockIndex == p) {
                       return reinterpret_cast<pointer>(p1);                        
                    }
                }
                p1 += abs(val) + sizeof(int)*2;
            }  
            assert(false);
            return 0;
        }
        // ----------
        // deallocate
        // ----------

        /**
         * O(1) in space
         * O(1) in time
         * after deallocation adjacent free blocks must be coalesced
         * throw an invalid_argument exception, if p is invalid
         * <your documentation>
         */
        void deallocate (pointer p, size_type s) {
            char* p1 =  reinterpret_cast<char*>(p);
            if (p1<a || p1 > (a+N-sizeof(int)*2-sizeof(T)))
                throw std::invalid_argument("incorrect argument");
            int val = get(*p1);
            if(val > 0)
                throw std::invalid_argument("this block cannot be deallocated");
            set(*p1, abs(val));
            set(*(p1 + abs(val) + sizeof(int)), abs(val));
            int check = checkCoalesce(p1);
            if(check == 3) {
                const int temp = get(*(p1-sizeof(int)));
                coalesce(p1 - temp - sizeof(int)*2,p1);
                coalesce(p1 - temp - sizeof(int)*2, p1 + abs(val) + sizeof(int)*2);
            } else if(check == 2) {
                coalesce(p1, p1 + get(*p1) + sizeof(int)*2);
            } else if(check == 1) {
                coalesce(p1 - get(*(p1-sizeof(int))) - sizeof(int)*2,p1);
            }
            assert(valid());
        }

        // -------
        // destroy
        // -------

        /**
         * O(1) in space
         * O(1) in time
         */
        void destroy (pointer p) {
            p->~T();               // this is correct
            assert(valid());}

        // -----------
        // operator []
        // -----------

        /**
         * O(1) in space
         * O(1) in time
         */
        int& operator [] (int i) {
            return *reinterpret_cast<int*>(&a[i]);}

        // -----------
        // operator []
        // -----------

        /**
         * O(1) in space
         * O(1) in time
         */
        const int& operator [] (int i) const {
            return *reinterpret_cast<const int*>(&a[i]);
        }

         //prints all the sentinals
        std::string printSentinels() {
            int index = 0;
            std::string result = "";
            while(index < N) {
                int val = this->operator[](index);
                result += std::to_string(val) + " ";
                index += abs(val) + sizeof(int)*2;
            }
            return result;
        }
    };

#endif // Allocator_h

using namespace std;
void allocator_solve(istream& r, ostream& w) {
    int numTestCases = 0;
    r>>numTestCases;
    string line = "";
    getline(r, line);
    getline(r, line);
    while(numTestCases > 0) {    
        my_allocator<double, 1000> a;
        bool last = true;
        while(getline(r, line)) {
            if(line.size() == 0) {
                --numTestCases;
                last = false;
                break;
            }
            int val = stoi(line);
            
            if (val > 0) {
                a.allocate(val);
            } else {
                a.deallocate(a.getNthBusyBlock(val), 0);
            }
            //w<<"cur state "<<a.printSentinels()<<endl;
        }
        if (last)
            --numTestCases;
         w<<a.printSentinels()<<endl;
    }
}