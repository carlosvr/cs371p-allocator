// ---------------------------------------
// projects/c++/allocator/RunAllocator.c++
// Copyright (C) 2018
// Glenn P. Downing
// ---------------------------------------

// --------
// includes
// --------

#include <cassert>  // assert
#include <fstream>  // ifstream
#include <iostream> // cout, endl, getline
#include <string>   // s

#include "Allocator.h"

// ----
// main
// ----

int main () {
    using namespace std;

    allocator_solve(cin, cout);

    // read  from stdin,  redirect from RunAllocator.in
    // write to   stdout, redirect to   RunAllocator.out

    return 0;}
